#include<iostream>
using namespace std;
int distance(float a1, float a2, float b1, float b2);
int main()
{
	int x1, y1, x2, y2;
	cout << "第一个点的坐标:";
	cin >> x1 >> y1;
	cout << "第二个点的坐标:";
	cin >> x2 >> y2;
	cout << "两点之间的距离为:" << distance(x1, x2, y1, y2) << endl;
	system("pause");
}
int distance(float a1, float a2, float b1, float b2)
{
	float c;
	c = sqrt(((a1 - a2)*(a1 - a2)) + (b1 - b2)*(b1 - b2));
	return c;
}