//用if语句即可
#include<iostream>
using namespace std;
int main()
{
	int x=0, y=0, z=0;//正数，负数，0的个数
	float a;//输入的数
	for (int i = 0; i < 20; i++)
	{
		cout << "请输入第" << i + 1 << "个数:\n";
		/*if (cin >> a&&a == 0)//错误的代码
		{
			x++;
		}
		else if (cin >> a&&a > 0)
		{
			y++;
		}
		else if (cin >> a&&a < 0)
		{
			z++;
		}*/

		if (cin >> a)
		{
			if (a == 0)
			{
				x++;
			}
			else if (a > 0)
			{
				y++;
			}
			else if (a < 0)
			{
				z++;
			}
		}	
	}
	cout << "0的个数为:" << x << endl;
	cout << "正数的个数为:" << y<<endl;
	cout << "负数的个数为:" << z << endl;
	system("pause");
}