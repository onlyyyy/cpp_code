//c++没有自带的求阶乘符号，所以要自己写一个函数去求阶乘
#include<iostream>
using namespace std;
int func(int x);
int main()
{
	int s=0;
	for (int i = 1; i <= 10; i++)
	{
		s += func(i);
	}
	cout << "从1到10的阶乘为:" << s << endl;
	system("pause");
}
int func(int x)
{
	int f=1;
	for (int i = 1; i <= x; i++)
	{
		f *= i;
	}
	return f;
}