#include<iostream>
#include<time.h>
using namespace std;
int main()
{
	while (1)
	{
		system("cls");
		srand(time(0));
		int a = rand() % 100;
		int b;
		cout << "请输入一个0-100的整数\n";
		cout << "这个数字是多少？输入-1退出程序\n";
		cin >> b;
		if (b == -1)
		{
			cout << "程序退出" << endl;
			system("pause");
			exit(0);
		}
		while (b != a)
		{
			if (b > a)
			{
				cout << "这个数字大了，请重新输入\n";
				cin >> b;
			}
			else if (b < a)
			{
				cout << "这个数字小了，请重新输入\n";
				cin >> b;
			}
		}
		if (b == a)
		{
			cout << "你猜对了！" << endl;
		}
		
		system("pause");
	}
}
