//题意是遍历100-999的所有水仙花数
#include<iostream>
#include<iomanip>
using namespace std;
int lifang(int a);
int main()
{
	
	int x, y, z;//个位十位百位
	cout << "水仙花数为:";
	for (int i = 100; i < 999; i++)
	{
		
		x = i/10 % 10;
		y = i/100 % 10;
		z = i % 10;
		if (i == lifang(x) + lifang(y) + lifang(z))
		{
			cout << left << setw(10) << i;
		}
	}
	system("pause");
}
int lifang(int a)
{
	int sum;
	sum = a*a*a;
	return sum;
}