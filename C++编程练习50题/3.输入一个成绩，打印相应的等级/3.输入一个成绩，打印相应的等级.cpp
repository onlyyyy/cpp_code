//用if else语句判断大小
#include<iostream>
using namespace std;
double GPA(double a);
int main()
{
	while (1)
	{
		double c;
		cout << "输入一个成绩,系统将返回相应的绩点\n";
		cin >> c;
		if (c >= 0 && c <= 100)
		{
			double l;
			l = GPA(c);

			cout << "绩点为:" << l << endl;

		}
		else
		{
			cout << "输入错误\n";
		}
	}
	system("pause");
}
double GPA(double a)
{
	double b;
	
	if (a >= 60 && a < 70)
	{
		b = 1;
		return b;
	}
	else if (a >= 70 && a < 80)
	{
		b = 2;
		return b;
	}
	else if (a >= 80 && a < 90)
	{
		b = 3;
		return b;
	}
	else if (a >= 90 && a <= 100)
	{
		b = 4;
		return b;
	}
	else if (a < 60&&a>=0)
	{
		b = 0;
		return b;
	}

}
