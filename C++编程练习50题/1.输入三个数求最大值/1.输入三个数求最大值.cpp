//用三目运算符?,先将两个数比较大小，传到一个新的变量中
//再把那两个数比较大小即可，输出就一个最大值
#include<iostream>
using namespace std;
int main()
{
	while (1)//测试用，使程序无限的循环下去
	{
		int a, b, c, d, e;
		cout << "请输入三个数，程序将输出他们的最大值\n";
		cout << "第一个数为:";
		cin >> a;
		cout << "第二个数为:";
		cin >> b;
		cout << "第三个数为:";
		cin >> c;
		d = a > b ? a : b;//意思是如果a>b，那么把a赋值给d，反之把b赋值给d
		e = c > d ? c : d;
		cout << "这三个数的最大值为:" << e << endl;
	}
	system("pause");
}