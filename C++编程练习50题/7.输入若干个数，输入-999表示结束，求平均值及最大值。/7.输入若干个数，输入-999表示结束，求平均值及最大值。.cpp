//可以考虑用if语句做
//个人理解：由于我们需要在循环里求输入了多少个数，在这之后才可能知道数组的长度
//而这个时候之前输入的东西是没地方保存的
//所以不用数组而当场把数据算出来就可以了，程序没要求输出我输入的数据
#include<iostream>
using namespace std;
int main()
{
	float num = 0;//记录输入了多少数
	float sum = 0;
	float x,aver,max;
	int i = 0;
	cout << "请输入若干个数，或者输入-999结束\n";
	while (cin >> x&&x!=-999)//当x输入了并且不等于-999
	{
		num++;
		sum += x;
		i++;
		if (i = 1)//这个if语句是为了处理max的问题，虽然用三目运算符，但是还是需要初始化
		{
			max = x;//若初始化为0，万一用户输入的全是负数，就会有bug，因此做一个简单的if语句即可
			
		}
		else
		{
			max = max > x ? max : x;
		}
	}
	
	aver = sum / num;
	cout << "平均值为:" << aver << endl;
	cout << "最大值为:" << max << endl;
	system("pause");
}