//求根判别式b^2-4*a*c
#include<iostream>
#include<math.h>
using namespace std;
int main()
{
	//先让用户输入方程的a，b，c
	cout << "请输入一元二次方程的a，b，c系数:";
	float a, b, c;//也许有小数点，或者分数
	cout << "a=";
	cin >> a;
	cout << "b=";
	cin >> b;
	cout << "c=";
	cin >> c;
	float D,x1,x2;
	float d;//D开根号之后存放在这里,也可以在if语句里直接调用sqrt函数
	D = b*b - 4 * a*c;
	d = sqrt(D);//开根号，需要配合头文件math.h使用
	if (D >= 0)
	{
		x1 = (-b + d) / 2 * a;
		x2 = (-b - d) / 2 * a;
		cout << "该方程实数根为:x1=" << x1 << "\nx2=" << x2 << endl;
	}
	else
	{
		cout << "该方程无实数根\n";//因为最后会有一个按任意键继续，这里多一个换行符会看起来美观点
	}
	system("pause");

}