﻿#ifndef INITIAL_H
#define INITIAL_H

#include <QMainWindow>

namespace Ui {
class initial;
}

class initial : public QMainWindow
{
    Q_OBJECT

public:
    explicit initial(QWidget *parent = 0);
    ~initial();

private slots:
    void on_pushButton_no_clicked();

    void on_pushButton_yes_clicked();
    void init_ini();//此函数对ini文件所有内容置0
private:
    Ui::initial *ui;
};

#endif // INITIAL_H
