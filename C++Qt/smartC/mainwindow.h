﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include "author.h"
#include <QMainWindow>
#include "cdex.h"
#include "headinfo.h"
#include"shujujiegou.h"
#include"initial.h"
#pragma execution_character_set("utf-8")
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pushButton_exit_clicked();
    void showauthor();


    void on_pushButton_language_clicked();

    void on_pushButton_head_clicked();

    void on_pushButton_shujujiegou_clicked();

    void on_pushButton_function_clicked();

    void on_pushButton_init_clicked();

    void on_pushButton_autocreate_clicked();

private:
    Ui::MainWindow *ui;
    author *a;
    initial *init;
    //cdex *Cdex;
    headinfo *head;

};

#endif // MAINWINDOW_H
