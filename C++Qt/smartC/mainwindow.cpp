﻿#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "author.h"
#include<QPixmap>
#include<QSettings>
#include<QDebug>
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    //setStyleSheet("border:2px groove gray;border-radius:10px;padding:2px 4px;");
    connect(ui->actionAuthor,SIGNAL(triggered(bool)),this,SLOT(showauthor()));
    connect(ui->actionC_dex,SIGNAL(triggered(bool)),this,SLOT(on_pushButton_language_clicked()));
    connect(ui->action_shuju,SIGNAL(triggered(bool)),this,SLOT(on_pushButton_shujujiegou_clicked()));
    connect(ui->action_head,SIGNAL(triggered(bool)),this,SLOT(on_pushButton_head_clicked()));
    connect(ui->action_function,SIGNAL(triggered(bool)),this,SLOT(on_pushButton_function_clicked()));

    QPixmap pixmap = QPixmap("./image/mainwindow.jpg").scaled(this->size());//窗口背景图片 始
    QPalette  palette (this->palette());
    palette.setBrush(QPalette::Background, QBrush(pixmap));
    this-> setPalette( palette );//窗口背景图片 终
    ui->pushButton_exit->setIcon(QIcon("./image/exit.ico"));//设置退出按钮的图标
    ui->pushButton_language->setIcon(QIcon("./image/opencdex.ico"));
    ui->pushButton_head->setIcon(QIcon("./image/openhead.ico"));
    ui->pushButton_shujujiegou->setIcon(QIcon("./image/openshuju.ico"));
    ui->pushButton_function->setIcon(QIcon("./image/openfunc.ico"));
    ui->actionC_dex->setIcon(QIcon("./image/opencdex.ico"));
    ui->action_head->setIcon(QIcon("./image/openhead.ico"));
    ui->action_shuju->setIcon(QIcon("./image/openshuju.ico"));
    ui->action_function->setIcon(QIcon("./image/openfunc.ico"));
    ui->actionAuthor->setIcon(QIcon("./image/author.ico"));
    ui->pushButton_autocreate->setIcon(QIcon("./image/create.ico"));
    ui->pushButton_init->setIcon(QIcon("./image/cle.ico"));
    setFixedSize(734,554);//设置窗口不可拉伸
    ui->pushButton_language->setStyleSheet("QPushButton{background-color:black;color: white;   border-radius: 10px;  border: 2px groove gray;border-style: outset;}"
                                           "QPushButton:hover{background-color:white; color: black;}"

                                              "QPushButton:pressed{background-color:rgb(85, 170, 255);border-style: inset; }"

                                               );
    ui->pushButton_head->setStyleSheet("QPushButton{background-color:black;color: white;   border-radius: 10px;  border: 2px groove gray;border-style: outset;}"
                                           "QPushButton:hover{background-color:white; color: black;}"

                                              "QPushButton:pressed{background-color:rgb(85, 170, 255);border-style: inset; }"

                                               );
    ui->pushButton_shujujiegou->setStyleSheet("QPushButton{background-color:black;color: white;   border-radius: 10px;  border: 2px groove gray;border-style: outset;}"
                                           "QPushButton:hover{background-color:white; color: black;}"

                                              "QPushButton:pressed{background-color:rgb(85, 170, 255);border-style: inset; }"

                                               );
    ui->pushButton_function->setStyleSheet("QPushButton{background-color:black;color: white;   border-radius: 10px;  border: 2px groove gray;border-style: outset;}"
                                           "QPushButton:hover{background-color:white; color: black;}"

                                              "QPushButton:pressed{background-color:rgb(85, 170, 255);border-style: inset; }"

                                               );
    ui->pushButton_exit->setStyleSheet("QPushButton{background-color:black;color:white;   border-radius: 10px;  border: 2px groove gray;border-style: outset;}"
                                           "QPushButton:hover{background-color:white; color: black;}"

                                              "QPushButton:pressed{background-color:rgb(85, 170, 255);border-style: inset; }"

                                               );//按钮美化 黑底白字，选中变白，点击变蓝
    ui->pushButton_init->setStyleSheet("QPushButton{background-color:black;color: white;   border-radius: 10px;  border: 2px groove gray;border-style: outset;}"
                                           "QPushButton:hover{background-color:white; color: black;}"

                                              "QPushButton:pressed{background-color:rgb(85, 170, 255);border-style: inset; }"

                                               );
    ui->pushButton_autocreate->setStyleSheet("QPushButton{background-color:black;color: white;   border-radius: 10px;  border: 2px groove gray;border-style: outset;}"
                                           "QPushButton:hover{background-color:white; color: black;}"

                                              "QPushButton:pressed{background-color:rgb(85, 170, 255);border-style: inset; }"

                                               );


}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_exit_clicked()
{
    exit(0);
}
void MainWindow::showauthor()
{
    a=new author;
    a->show();

}

void MainWindow::on_pushButton_language_clicked()
{
    author a;
    a.cdexshow();
}

void MainWindow::on_pushButton_head_clicked()
{
    author a;
    a.headinfoshow();
}

void MainWindow::on_pushButton_shujujiegou_clicked()
{
    author a;
    a.shujushow();
}


void MainWindow::on_pushButton_function_clicked()
{
    author a;
    a.funcshow();
}

void MainWindow::on_pushButton_init_clicked()
{
    init=new initial;
    init->show();
}

void MainWindow::on_pushButton_autocreate_clicked()
{
    author a;
    a.createshow();
}
