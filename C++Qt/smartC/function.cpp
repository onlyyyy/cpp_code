﻿#include "function.h"
#include "ui_function.h"
#include<QSqlQueryModel>
#include"author.h"
#include<QSettings>
#pragma execution_character_set("utf-8")//设置中文= =
#include<QMessageBox>
function::function(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::function)
{
    ui->setupUi(this);
    connect(ui->actionC_cdex,SIGNAL(triggered(bool)),this,SLOT(showyufa()));
    connect(ui->action_shuju,SIGNAL(triggered(bool)),this,SLOT(showshuju()));
    connect(ui->action_head,SIGNAL(triggered(bool)),this,SLOT(showhead()));
    connect(ui->action_create,SIGNAL(triggered(bool)),this,SLOT(showcreate()));
    ui->tableView->verticalHeader()->hide();
    ui->tableView->horizontalHeader()->setSectionsClickable(false);
    ui->tableView->horizontalHeader()->setStretchLastSection(true);
    ui->tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->tableView->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->tableView->setVerticalScrollMode(QAbstractItemView::ScrollPerPixel);
    ui->tableView->setHorizontalScrollMode(QAbstractItemView::ScrollPerPixel);
    ui->tableView->setShowGrid(false);  // disable the table grid.
    ui->tableView->verticalHeader()->setDefaultSectionSize(25);  // set row height.
    ui->tableView->horizontalHeader()->setHighlightSections(false);
    ui->tableView->setFrameShape(QFrame::NoFrame);
    QPixmap pixmap = QPixmap("./image/function.jpg").scaled(this->size());//窗口背景图片 始
    QPalette  palette (this->palette());
    palette .setBrush(QPalette::Background, QBrush(pixmap));
    this-> setPalette( palette );//窗口背景图片 终
    ui->pushButton_exit->setIcon(QIcon("./image/exit.ico"));//设置退出按钮的图标
    init();

    setFixedSize(800,600);//设置窗口不可拉伸
    ui->actionC_cdex->setIcon(QIcon("./image/opencdex.ico"));
    ui->action_head->setIcon(QIcon("./image/openhead.ico"));
    ui->action_shuju->setIcon(QIcon("./image/openshuju.ico"));
    ui->action_create->setIcon(QIcon("./image/create.ico"));
    ui->pushButton_exit->setStyleSheet("QPushButton{background-color:black;color: white;   border-radius: 10px;  border: 2px groove gray;border-style: outset;}"
                                           "QPushButton:hover{background-color:white; color: black;}"

                                              "QPushButton:pressed{background-color:rgb(85, 170, 255);border-style: inset; }"

                                               );//按钮美化 黑底白字，选中变白，点击变蓝
    invite_judge();
}

function::~function()
{
    delete ui;
}
void function::init()
{
    QSqlQueryModel *model=new QSqlQueryModel;
    model->setQuery("select name as'名称' from function order by name");
    ui->tableView->setModel(model);
}

void function::on_pushButton_exit_clicked()
{
    close();


}
void function::Show()
{
     QVariant currentData=ui->tableView->currentIndex().data();
     QSettings a("./data.ini",QSettings::IniFormat);
     if(currentData=="无参函数")
     {
         voidfunc();
         QString info=a.value("/function/wucanhanshu").toString();
         int INFO;
         INFO=info.toInt();
         INFO++;
         a.beginGroup("function");
         a.setValue("wucanhanshu",INFO);
         a.endGroup();
     }
     else if(currentData=="带参函数")
     {
         havefunc();
         QString info=a.value("/function/daicanhanshu").toString();
         int INFO;
         INFO=info.toInt();
         INFO++;
         a.beginGroup("function");
         a.setValue("daicanhanshu",INFO);
         a.endGroup();
     }
     else if(currentData=="引用")
     {
         YINYONG();
         QString info=a.value("/function/yinyong").toString();
         int INFO;
         INFO=info.toInt();
         INFO++;
         a.beginGroup("function");
         a.setValue("yinyong",INFO);
         a.endGroup();
     }
     else if(currentData=="指针函数")
     {
         ZHIZHENFUNC();
         QString info=a.value("/function/zhizhenhanshu").toString();
         int INFO;
         INFO=info.toInt();
         INFO++;
         a.beginGroup("function");
         a.setValue("zhizhenhanshu",INFO);
         a.endGroup();
     }
     else if(currentData=="递归函数")
     {
         DIGUI();
         QString info=a.value("/function/diguihanshu").toString();
         int INFO;
         INFO=info.toInt();
         INFO++;
         a.beginGroup("function");
         a.setValue("diguihanshu",INFO);
         a.endGroup();
     }
     else if(currentData=="虚函数")
     {
         XUHANSHU();
         QString info=a.value("/function/xuhanshu").toString();
         int INFO;
         INFO=info.toInt();
         INFO++;
         a.beginGroup("function");
         a.setValue("xuhanshu",INFO);
         a.endGroup();
     }
     else if(currentData=="纯虚函数")
     {
         CHUNXUHANSHU(); QString info=a.value("/function/chunxuhanshu").toString();
         int INFO;
         INFO=info.toInt();
         INFO++;
         a.beginGroup("function");
         a.setValue("chunxuhanshu",INFO);
         a.endGroup();

     }
     else if(currentData=="基类")
     {
         JILEI();
         QString info=a.value("/function/jilei").toString();
         int INFO;
         INFO=info.toInt();
         INFO++;
         a.beginGroup("function");
         a.setValue("jilei",INFO);
         a.endGroup();
     }
     else if(currentData=="友元")
     {
         FRIEND();
         QString info=a.value("/function/youyuan").toString();
         int INFO;
         INFO=info.toInt();
         INFO++;
         a.beginGroup("function");
         a.setValue("youyuan",INFO);
         a.endGroup();
     }
     else if(currentData=="类")
     {
         CLASS();
         QString info=a.value("/function/leii").toString();
         int INFO;
         INFO=info.toInt();
         INFO++;
         a.beginGroup("function");
         a.setValue("lei",INFO);
         a.endGroup();
     }
     else if(currentData=="构造函数")
     {
         GOUZAO();
         QString info=a.value("/function/gouzaohanshu").toString();
         int INFO;
         INFO=info.toInt();
         INFO++;
         a.beginGroup("function");
         a.setValue("gouzaohanshu",INFO);
         a.endGroup();
     }
     else if(currentData=="析构函数")
     {
         XIGOU();
         QString info=a.value("/function/xigouhanshu").toString();
         int INFO;
         INFO=info.toInt();
         INFO++;
         a.beginGroup("function");
         a.setValue("xigouhanshu",INFO);
         a.endGroup();
     }
     else if(currentData=="重载")
     {
         CHONGZAI();
         QString info=a.value("/function/chongzai").toString();
         int INFO;
         INFO=info.toInt();
         INFO++;
         a.beginGroup("function");
         a.setValue("chongzai",INFO);
         a.endGroup();
     }
}
void function::voidfunc()
{
    ui->textEdit->setText("");
    ui->textEdit->append("如果函数无参数,那么应声明其参数为 void ");
    ui->textEdit->append("代码示例:");
    ui->textEdit->append("void menu()");
    ui->textEdit->append("{");
    ui->textEdit->append("  cout<<""hello""<<endl;");
    ui->textEdit->append("}");
    ui->textEdit->append("主函数调用:");
    ui->textEdit->append("menu();");
    ui->textEdit->append("这就完成了一个无参函数的声明和调用");


}

void function::on_tableView_clicked(const QModelIndex &index)
{
    Show();
}
void function::havefunc()
{
    ui->textEdit->setText("");
    ui->textEdit->append("C++函数传递参数的方式有以下四种:");
    ui->textEdit->append("(1) 值传递");
    ui->textEdit->append("当进行值传递时，就是将实参的值复制到形参中，而形参和实参不是同一个存储单元，所以函数调用结束后，实参的值不会发生改变，程序示例如下：");
    ui->textEdit->append("#include<iostream>");
    ui->textEdit->append("using namespace std;");
    ui->textEdit->append("void swap(int a,int b){");
    ui->textEdit->append("int temp;");
    ui->textEdit->append("temp =a;");
    ui->textEdit->append("a=b;");
    ui->textEdit->append("b = temp;");
    ui->textEdit->append("}");
    ui->textEdit->append("当传进去a,b的时候，在参数外面打印参数值会发现调用swap方法之后参数的值没有发生变化；");
    ui->textEdit->append("(2)指针传递");
    ui->textEdit->append("当进行指针传递时，形参是指针变量，实参是一个变量的地址，调用函数时，形参(指针变量)指向实参变量单元，这种方式还是""值传递""，");
    ui->textEdit->append("只不过实参的值是变量的地址而已，而在函数中改变的不是实参的值，而是实参变量地址所指向的变量的值；");
    ui->textEdit->append("(3)传引用");
    ui->textEdit->append("实参地址传递到形参，使形参的地址取实参的地址，从而使形参与实参共享同一单元的方式；");
    ui->textEdit->append("(4)全局变量传递");
    ui->textEdit->append("这里的“全局”变量并不见得就是真正的全局的，所有的代码都可以直接访问的，只要这个变量的作用域足够这两个函数访问就可以了，比如一个类中的两个成员函数可以使用一个成员变量实现参数传递，");
    ui->textEdit->append("或者使用static关键字定义，或者使用namespace进行限制等，而这里的成员变量在这种意义上就可以称为""全局""变量。");
    ui->textEdit->append("当然，可以使用类外一个的真正的全局变量来实现参数传递，但是有时并没有必要，从工程上讲，作用域越小越好。");
    ui->textEdit->append("全局变量的优点是效率高，但它对多线程的支持不好，如果两个进程同时调用同一个函数，而通过全局变量进行传递参数，该函数就不能总是得到想要的结果。");
    ui->textEdit->append("C++ 带默认形参值的函数:");
    ui->textEdit->append("函数声明或者定义的时候，可以给形参赋一些默认值。调用函数时，若没有给出实参，则按指定的默认值进行工作。");
    ui->textEdit->append("应用:");
    ui->textEdit->append("(1)、函数没有声明时，在函数定义中指定形参的默认值。");
    ui->textEdit->append("(2)、函数既有定义又有声明时，声明时指定后，定义后就不能再指定默认值。");
    ui->textEdit->append("(3)、默认值的定义必须遵守从右到左的顺序，如果某个形参没有默认值，则它左边的参数就不能有默认值。");
    ui->textEdit->append(" –void func1(int a, double b=4.5,int c=3); //合法");
    ui->textEdit->append(" –void func1(int a=1, double b,int c=3);  //不合法");
    ui->textEdit->append("(4)、函数调用时，实参与形参按从左到右的顺序进行匹配。");
    ui->textEdit->append("(5)、重载的函数中如果形参带有默认值时，可能产生二义性。例子如下：");
    ui->textEdit->append("int add(int x=5,int y=6);");
    ui->textEdit->append("float add(int x=5, float y=10.0);");
    ui->textEdit->append("在main函数调用时");
    ui->textEdit->append("a= add(10,20);");
    ui->textEdit->append("b= add(10);    将产生二义性   ");
    ui->textEdit->moveCursor(QTextCursor::Start,QTextCursor::MoveAnchor);

}
void function::YINYONG()
{
    ui->textEdit->setText("");
    ui->textEdit->append("引用引入了对象的一个同义词。定义引用的表示方法与定义指针相似，只是用&代替了*。引用（reference）是c++对c语言的重要扩充。");
    ui->textEdit->append("引用就是某一变量（目标）的一个别名，对引用的操作与对变量直接操作完全一样。引用的声明方法：类型标识符 &引用名=目标变量名；");
    ui->textEdit->append("引用说明:");
    ui->textEdit->append("（1）&在此不是求地址运算，而是起标识作用。");
    ui->textEdit->append("（2）类型标识符是指目标变量的类型。");
    ui->textEdit->append("（3）声明引用时，必须同时对其进行初始化。");
    ui->textEdit->append("（4）引用声明完毕后，相当于目标变量名有两个名称，即该目标原名称和引用名，且不能再把该引用名作为其他变量名的别名。");
    ui->textEdit->append("int a=2,&ra=a;");
    ui->textEdit->append("a为目标原名称，ra为目标引用名。给ra赋值：ra=1; 等价于 a=1;");
    ui->textEdit->append("5）对引用求地址，就是对目标变量求地址。&ra与&a相等。即我们常说引用名是目标变量名的一个别名。别名一词好像是说引用不占据任何内存空间。");
    ui->textEdit->append("但是编译器在一般将其实现为const指针，即指向位置不可变的指针。即引用实际上与一般指针同样占用内存。");
    ui->textEdit->append("（6）不能建立引用的数组。因为数组是一个由若干个元素所组成的集合，所以无法建立一个由引用组成的集合。但是可以建立数组的引用.");
    ui->textEdit->append("例如： int& ref [3]= {2,3,5};//声明ref引用的数组错误");
    ui->textEdit->append("但是可以这样写:");
    ui->textEdit->append("const int (&ref)[3] ={2,3,5}; //gcc编译的时候加上选项 -std=c++0x");
    ui->textEdit->append("ref[0] = 35; //错误");
    ui->textEdit->append("为什么要加上const ,因为{2,3,5}此时是个字面值数组,是保存在代码段里,只读的属性,如果不加,编译错误,而且后面对ref[0]的赋值也不会成功.");
    ui->textEdit->append("需要特别强调的是引用并不产生对象的副本，仅仅是对象的同义词。因此，当下面的语句执行后：");
    ui->textEdit->append("pt1.offset（12，12）；");
    ui->textEdit->append("pt1和pt2都具有（12，12）的值。");
    ui->textEdit->append("引用必须在定义时马上被初始化，因为它必须是某个东西的同义词。你不能先定义一个引用后才");
    ui->textEdit->append("引用参数:");
    ui->textEdit->append("1、传递可变参数");
    ui->textEdit->append("传统的c中，函数在调用时参数是通过值来传递的，这就是说函数的参数不具备返回值的能力。");
    ui->textEdit->append("所以在传统的c中，如果需要函数的参数具有返回值的能力，往往是通过指针来实现的。");
    ui->textEdit->append("void swapint(int *a,int *b)");
    ui->textEdit->append("{");
    ui->textEdit->append("int temp;");
    ui->textEdit->append("temp=*a;");
    ui->textEdit->append("*a=*b;");
    ui->textEdit->append("*b=temp;");
    ui->textEdit->append("}");
    ui->textEdit->append("比如，实现两整数变量值交换的c程序如下：");
    ui->textEdit->append("2、给函数传递大型对象");
    ui->textEdit->append("当大型对象被传递给函数时，使用引用参数可使参数传递效率得到提高，因为引用并不产生对象的副本，也就是参数传递时，对象无须复制。");
    ui->textEdit->moveCursor(QTextCursor::Start,QTextCursor::MoveAnchor);


}
void function::ZHIZHENFUNC()
{
    ui->textEdit->setText("");
    ui->textEdit->append("对于函数的表达形式我们知道是： ");
    ui->textEdit->append("返回类型 函数名（参数）； 这一种表达形式，比如现在定义一个函数，其形式如下：");
    ui->textEdit->append("int max(int a, int b);                        //1-1");
    ui->textEdit->append("从上方的形式可以得到这是一个返回类型为 int型 函数名为 max 函数的参数有两个，并且其类型都是 int型 这时候如果在函数名字的前面加上 ” * ” 这个符号，得到的形式如下：");
    ui->textEdit->append("int* max(int a, int b);                       //1-2");
    ui->textEdit->append("表达式1-1与表达式1-2进行对比后发现，表达式1-2比表达式1-1多了一个“ * ”,但是他们的意义却完全不同，函数表达式1-2得到的是一个函数名为max，其含有两个参数，参数类型都是int型，");
    ui->textEdit->append("但是其返回类型不再是int型而是一个int*（即：函数max的返回类型为一个指向int型的指针。对于1-2所述的表达式就是一个指针型函数。");
    ui->textEdit->append("函数名称前的修饰类型决定了此函数的返回值类型");
    ui->textEdit->moveCursor(QTextCursor::Start,QTextCursor::MoveAnchor);
}
void function::DIGUI()
{
    ui->textEdit->setText("");
    ui->textEdit->append("在调用一个函数的过程中又出现直接或间接地调用该函数本身，称为函数的递归(recursive)调用。包含递归调用的函数称为递归函数。");
    ui->textEdit->append("比如：");
    ui->textEdit->append("int test(int x)");
    ui->textEdit->append("{");
    ui->textEdit->append("int y;");
    ui->textEdit->append("y = test(x);");
    ui->textEdit->append("return(2*y);");
    ui->textEdit->append("以上是一个直接调用的例子，递归调用还包括间接调用，比如：");
    ui->textEdit->append("int first(int x)");
    ui->textEdit->append("{");
    ui->textEdit->append("int b;");
    ui->textEdit->append("b = second(x);");
    ui->textEdit->append("return(2*b);");
    ui->textEdit->append("}");
    ui->textEdit->append("int second(int y)");
    ui->textEdit->append("{");
    ui->textEdit->append("int a;");
    ui->textEdit->append("a = first(y);");
    ui->textEdit->append("return(2*a);");
    ui->textEdit->append("}");
    ui->textEdit->moveCursor(QTextCursor::Start,QTextCursor::MoveAnchor);
}
void function::XUHANSHU()
{
    ui->textEdit->setText("");
    ui->textEdit->append("虚函数的使用方法：");
    ui->textEdit->append("（1）在基类用virtual声明成员函数为虚函数。这样就可以在派生类中重新定义此函数，为它赋予新的功能，并能方便被调用。");
    ui->textEdit->append("在类外定义虚函数时，不必再定义virtual");
    ui->textEdit->append("（2）在派生类中重新定义此函数，要求函数名，函数类型，函数参数个数和类型全部与基类的虚函数相同，并根据派生类的需要重新定义函数体。");
    ui->textEdit->append("（3）定义一个指向基类对象的指针变量，并使它指向同一类族中需要调用该函数的对象。");
    ui->textEdit->append("（4）通过该指针变量调用此虚函数，此时调用的就是指针变量指向的对象的同名函数。");
    ui->textEdit->append("简单地说，那些被virtual关键字修饰的成员函数，就是虚函数。");
    ui->textEdit->append("虚函数的作用，用专业术语来解释就是实现多态性（Polymorphism），多态性是将接口与实现进行分离；");
    ui->textEdit->append("用形象的语言来解释就是实现以共同的方法，但因个体差异，而采用不同的策略。");
    ui->textEdit->append("定义虚函数的限制：");
    ui->textEdit->append("（1）非类的成员函数不能定义为虚函数，类的成员函数中静态成员函数和构造函数也不能定义为虚函数，但可以将析构函数定义为虚函数。");
    ui->textEdit->append("实际上，优秀的程序员常常把基类的析构函数定义为虚函数。因为，将基类的析构函数定义为虚函数后，");
    ui->textEdit->append("当利用delete删除一个指向派生类定义的对象指针时，系统会调用相应的类的析构函数。而不将析构函数定义为虚函数时，只调用基类的析构函数。");
    ui->textEdit->append("（2）只需要在声明函数的类体中使用关键字“virtual”将函数声明为虚函数，而定义函数时不需要使用关键字“virtual”。");
    ui->textEdit->append("（3）当将基类中的某一成员函数声明为虚函数后，派生类中的同名函数（函数名相同、参数列表完全一致、返回值类型相关）自动成为虚函数。");
    ui->textEdit->append("（4）如果声明了某个成员函数为虚函数，则在该类中不能出现和这个成员函数同名并且返回值、参数个数、类型都相同的非虚函数。");
    ui->textEdit->append("在以该类为基类的派生类中，也不能出现和这个成员函数同名并且返回值、参数个数、类型都相同的非虚函数。");
    ui->textEdit->moveCursor(QTextCursor::Start,QTextCursor::MoveAnchor);
}
void function::CHUNXUHANSHU()
{
    ui->textEdit->setText("");
    ui->textEdit->append("纯虚函数是一种特殊的虚函数，在许多情况下，在基类中不能对虚函数给出有意义的实现，而把它声明为纯虚函数，它的实现留给该基类的派生类去做。这就是纯虚函数的作用。");
    ui->textEdit->append("纯虚函数它的一般格式如下：");
    ui->textEdit->append("class <类名>");
    ui->textEdit->append("{");
    ui->textEdit->append("virtual <类型><函数名>(<参数表>)=0;");
    ui->textEdit->append("...");
    ui->textEdit->append("};");
    ui->textEdit->append("在许多情况下，在基类中不能对虚函数给出有意义的实现，而把它声明为纯虚函数，它的实现留给该基类的派生类去做。这就是纯虚函数的作用。");
    ui->textEdit->append("纯虚函数可以让类先具有一个操作名称，而没有操作内容，让派生类在继承时再去具体地给出定义。凡是含有纯虚函数的类叫做抽象类。");
    ui->textEdit->append("这种类不能声明对象，只是作为基类为派生类服务。除非在派生类中完全实现基类中所有的的纯虚函数，否则，派生类也变成了抽象类，不能实例化对象。");
    ui->textEdit->append("一般而言纯虚函数的函数体是缺省的，但是也可以给出纯虚函数的函数体（此时纯虚函数变为虚函数），这一点经常被人们忽视，调用纯虚函数的方法为baseclass::virtual function.");
    ui->textEdit->append("引入原因:");
    ui->textEdit->append("1、为了方便使用多态特性，我们常常需要在基类中定义虚函数。");
    ui->textEdit->append("2、在很多情况下，基类本身生成对象是不合情理的。例如，动物作为一个基类可以派生出老虎、孔雀等子类，但动物本身生成对象明显不合常理。");
    ui->textEdit->append("为了解决上述问题，引入了纯虚函数的概念，将函数定义为纯虚函数（方法：virtual ReturnType Function()= 0;）。若要使派生类为非抽象类，则编译器要求在派生类中，必须对纯虚函数予以重写以实现多态性。");
    ui->textEdit->append("同时含有纯虚函数的类称为抽象类，它不能生成对象。这样就很好地解决了上述两个问题。");
    ui->textEdit->moveCursor(QTextCursor::Start,QTextCursor::MoveAnchor);

}
void function::JILEI()
{
    ui->textEdit->setText("");
    ui->textEdit->append("在面向对象设计中，被定义为包含所有实体共性的class类型，被称为“基类”。");
    ui->textEdit->append("继承性是面向对象程序设计的一个最重要的概念。继承性允许在构成软件系统的层次结构中利用已经存在的类并扩充它们，以支持新的功能。");
    ui->textEdit->append("这使得编程者只需要在新类中定义已经存在的类中所没有的成分来建立新类，从而大大提高了软件的可重用性和可维护性。");
    ui->textEdit->append("对于客观世界中既有共性又有差别的两个类别以上的实体是不可能被抽象成一个class类型来描述的，编程者往往采用继承的方法。");
    ui->textEdit->append("首先定义一个包含所有实体共性的class类型作为“基类”，然后，从该基类中继承所有信息，再添加新的信息，来构成新的类。");
    ui->textEdit->append("构建新类的过程中，新建立的类被称为“子类”或者“派生类”；而被继承的包含相同特征的类称为“父类”或者“基类”。");
    ui->textEdit->append("派生类继承了基类的全部成员，并且可以增加基类所没有的数据成员和成员函数，以满足描述新对象的需求。");
    ui->textEdit->append("");
    ui->textEdit->append("相关关系说明:");
    ui->textEdit->append("1.基类，父类，超类是指被继承的类，派生类，子类是指继承于基类的类．");
    ui->textEdit->append("2.在C++中使用：冒号表示继承，如class A:public B；表示派生类A从基类B继承而来");
    ui->textEdit->append("3.派生类包含基类的所有成员，而且还包括自已特有的成员，派生类和派生类对象访问基类中的成员就像访问自已的成员一样，可以直接使用，不需加任何操作符，但派生类仍然无法访问基类中的私有成员．");
    ui->textEdit->append("4.在C++中派生类可以同时从多个基类继承，Java不充许这种多重继承，当继承多个基类时，使用逗号将基类隔开．");
    ui->textEdit->append("5.基类访问控制符，class A:public B 基类以公有方式被继承，A:private B 基类以私有方式被继承，A:protected B 基类以受保护方式被继承，如果没有访问控制符则默认为私有继承。");
    ui->textEdit->append("6.protected 受保护的访问权限；使用protected 保护权限表明这个成员是私有的，但在派生类中可以访问基类中的受保护成员。派生类的对象就不能访问受保护的成员了。");
    ui->textEdit->append("7.如果基类以public 公有方式被继承，则基类的所有公有成员都会成为派生类的公有成员．受保护的基类成员成为派生类的受保护成员.");
    ui->textEdit->append("8.如果基类以private 私有被继承，则基类的所有公有成员都会成为派生类的私有成员．基类的受保护成员成为派生类的私有成员．");
    ui->textEdit->append("9.如果基类以protected 受保护方式被继承，那么基类的所有公有和受保护成员都会变成派生类的受保护成员。");
    ui->textEdit->append("10.不管基类以何种方式被继承，基类的私有成员，仍然保有其私有性，被派生的子类不能访问基类的私有成员.");
    ui->textEdit->moveCursor(QTextCursor::Start,QTextCursor::MoveAnchor);

}
void function::FRIEND()
{
    ui->textEdit->setText("");
    ui->textEdit->append("友元可以是一个函数，该函数被称为友元函数；友元也可以是一个类，该类被称为友元类。");
    ui->textEdit->append("我们已知道类具有封装和信息隐藏的特性。只有类的成员函数才能访问类的私有成员，程序中的其他函数是无法访问私有成员的。非成员函数可以访问类中的公有成员，但是如果将数据成员都定义为公有的，这又破坏了隐藏的特性。");
    ui->textEdit->append("另外，应该看到在某些情况下，特别是在对某些成员函数多次调用时，由于参数传递，类型检查和安全性检查等都需要时间开销，而影响程序的运行效率。");
    ui->textEdit->append("为了解决上述问题，提出一种使用友元的方案。友元是一种定义在类外部的普通函数或类，但它需要在类体内进行说明，为了与该类的成员函数加以区别，在说明时前面加以关键字friend。");
    ui->textEdit->append("友元不是成员函数，但是它可以访问类中的私有成员。友元的作用在于提高程序的运行效率，但是，它破坏了类的封装性和隐藏性，");
    ui->textEdit->append("使得非成员函数可以访问类的私有成员。不过，类的访问权限确实在某些应用场合显得有些呆板，从而容忍了友元这一特别语法现象。");
    ui->textEdit->append("友元函数是能够访问类中的私有成员的非成员函数。友元函数从语法上看，它与普通函数一样，即在定义上和调用上与普通函数一样。");
    ui->textEdit->append("友元关系不具对称性。即 A 是 B 的友元，但 B 不一定是 A 的友元。 友元关系不具传递性。即 B 是 A 的友元，C 是 B 的友元，但是 C 不一定是 A 的友元。");
    ui->textEdit->append("");
    ui->textEdit->append("友元类:");
    ui->textEdit->append("友元除了前面讲过的函数以外，友元还可以是类，即一个类可以作另一个类的友元。当一个类作为另一个类的友元时，");
    ui->textEdit->append("这就意味着这个类的所有成员函数都是另一个类的友元函数，都可以访问另一个类中的隐藏信息（包括私有成员和保护成员）。");
    ui->textEdit->append("定义友元类的语句格式如下：");
    ui->textEdit->append("friend class 类名（即友元类的类名）;");
    ui->textEdit->append("");
    ui->textEdit->append("注意事项:");
    ui->textEdit->append("(1) 友元关系不能被继承。");
    ui->textEdit->append("(2) 友元关系是单向的，不具有交换性。若类B是类A的友元，类A不一定是类B的友元，要看在类中是否有相应的声明。");
    ui->textEdit->append("(3) 友元关系不具有传递性。若类B是类A的友元，类C是B的友元，类C不一定是类A的友元，同样要看类中是否有相应的申明。");
    ui->textEdit->moveCursor(QTextCursor::Start,QTextCursor::MoveAnchor);
}
void function::CLASS()
{
    ui->textEdit->setText("");
    ui->textEdit->append("类（Class）是面向对象程序设计（OOP，Object-Oriented Programming）实现信息封装的基础。类是一种用户定义类型，也称类类型。");
    ui->textEdit->append("每个类包含数据说明和一组操作数据或传递消息的函数。类的实例称为对象。");
    ui->textEdit->append("类是面向对象程序设计中的概念，是面向对象编程的基础。");
    ui->textEdit->append("类的实质是一种数据类型，类似于int、char等基本类型，不同的是它是一种复杂的数据类型。");
    ui->textEdit->append("因为它的本质是类型，而不是数据，所以不存在于内存中，不能被直接操作，只有被实例化为对象时，才会变得可操作。");
    ui->textEdit->append("类是对现实生活中一类具有共同特征的事物的抽象。如果一个程序里提供的类型与应用中的概念有直接的对应，这个程序就会更容易理解，也更容易修改。");
    ui->textEdit->append("一组经过很好选择的用户定义的类会使程序更简洁。此外，它还能使各种形式的代码分析更容易进行。特别地，它还会使编译器有可能检查对象的非法使用。");
    ui->textEdit->append("");
    ui->textEdit->append("用法:");
    ui->textEdit->append("定义一个类");
    ui->textEdit->append("class类名");
    ui->textEdit->append("{");
    ui->textEdit->append("public:");
    ui->textEdit->append("公有成员");
    ui->textEdit->append("private:");
    ui->textEdit->append("私有成员");
    ui->textEdit->append("protected:");
    ui->textEdit->append("保护成员");
    ui->textEdit->append("};");
    ui->textEdit->append("1.公有成员、私有成员、保护成员均包含数据成员和成员函数两部分，彼此没有顺序之分。一个public/private/protected关键字下可以跟多个成员，直到下一个public/private/protected关键字。");
    ui->textEdit->append("如果成员前面没有public/private/protected关键字，默认为私有成员。结尾部分的分号必不可少，否则会发生编译错误。");
    ui->textEdit->append("2.无论公有成员、私有成员还是保护成员，彼此之间都可以访问。比如公有的成员函数可以操作保护的数据成员，也可以调用私有的成员函数。");
    ui->textEdit->append("3.类的数据成员是类型，所以不能被赋值，声明数据成员和声明普通变量的格式相同，比如“int n;”。");
    ui->textEdit->append("");
    ui->textEdit->append("成员函数的实现:");
    ui->textEdit->append("成员函数可以在类内实现，也可以在类外实现。内部实现的成员函数被默认为加上了inline；外部实现的成员函数必须加上域操作符，即“类名::成员函数”。");
    ui->textEdit->append("");
    ui->textEdit->append("构造函数与析构函数:");
    ui->textEdit->append("构造函数和析构函数是特殊的成员函数，和普通成员函数不同的地方在于：");
    ui->textEdit->append("1.构造函数和析构函数的函数名必须是类名。");
    ui->textEdit->append("2.构造函数和析构函数没有返回值，连空返回值——void也没有。");
    ui->textEdit->append("构造函数的声明形式：类名(参数列表);析构函数的声明形式：~类名();");
    ui->textEdit->append("3.重载的特殊性:构造函数和普通成员函数一样可以被重载，析构函数不可以重载，只能是空参数。");
    ui->textEdit->append("类的实例化:");
    ui->textEdit->append("类名 对象名(参数列表);");
    ui->textEdit->append("如果没有参数，括号必须省略，即“类名 对象名;”，自动调用构造函数。特殊地，参数可以是类的对象，此时会自动调用拷贝构造函数。");
    ui->textEdit->moveCursor(QTextCursor::Start,QTextCursor::MoveAnchor);


}
void function::GOUZAO()
{
    ui->textEdit->setText("");
    ui->textEdit->append("构造函数 ，是一种特殊的方法。主要用来在创建对象时初始化对象， 即为对象成员变量赋初始值，总与new运算符一起使用在创建对象的语句中。");
    ui->textEdit->append("特别的一个类可以有多个构造函数 ，可根据其参数个数的不同或参数类型的不同来区分它们 即构造函数的重载。");
    ui->textEdit->append("");
    ui->textEdit->append("主要特点:");
    ui->textEdit->append("1.构造函数的命名必须和类名完全相同。在java中普通函数可以和构造函数同名，但是必须带有返回值；");
    ui->textEdit->append("2.构造函数的功能主要用于在类的对象创建时定义初始化的状态。它没有返回值，也不能用void来修饰。这就保证了它不仅什么也不用自动返回，而且根本不能有任何选择。");
    ui->textEdit->append("而其他方法都有返回值，即使是void返回值。尽管方法体本身不会自动返回什么，但仍然可以让它返回一些东西，而这些东西可能是不安全的；");
    ui->textEdit->append("3.构造函数不能被直接调用，必须通过new运算符在创建对象时才会自动调用；而一般的方法是在程序执行到它的时候被调用的；");
    ui->textEdit->append("4.当定义一个类的时候，通常情况下都会显示该类的构造函数，并在函数中指定初始化的工作也可省略.");
    ui->textEdit->append("5.构造函数有回滚的效果，构造函数抛出异常时，构造的是一个不完整对象，会回滚，将此不完整对象的成员释放");
    ui->textEdit->append("6.当一个类只定义了私有的构造函数，将无法通过new关键字来创建其对象，当一个类没有定义任何构造函数，C#编译器会为其自动生成一个默认的无参的构造函数。");
    ui->textEdit->moveCursor(QTextCursor::Start,QTextCursor::MoveAnchor);
}
void function::XIGOU()
{
    ui->textEdit->setText("");
    ui->textEdit->append("析构函数(destructor) 与构造函数相反，当对象结束其生命周期时（例如对象所在的函数已调用完毕），系统自动执行析构函数。");
    ui->textEdit->append("析构函数往往用来做“清理善后” 的工作（例如在建立对象时用new开辟了一片内存空间，delete会自动调用析构函数后释放内存）。");
    ui->textEdit->append("析构函数名也应与类名相同，只是在函数名前面加一个位取反符~，例如~stud( )，以区别于构造函数。");
    ui->textEdit->append("它不能带任何参数，也没有返回值（包括void类型）。只能有一个析构函数，不能重载。");
    ui->textEdit->append("如果用户没有编写析构函数，编译系统会自动生成一个缺省的析构函数（即使自定义了析构函数，编译器也总是会为我们合成一个析构函数，");
    ui->textEdit->append("并且如果自定义了析构函数，编译器在执行时会先调用自定义的析构函数再调用合成的析构函数）.");
    ui->textEdit->append("它也不进行任何操作。所以许多简单的类中没有用显式的析构函数。");
    ui->textEdit->append("");
    //文本量不大不用移动光标
}
void function::CHONGZAI()
{
    ui->textEdit->setText("");
    ui->textEdit->append("重载是可使函数、运算符等处理不同类型数据或接受不同个数的参数的一种方法.");
    ui->textEdit->append("重载函数是函数的一种特殊情况，为方便使用，C++允许在同一范围中声明几个功能类似的同名函数");
    ui->textEdit->append("但是这些同名函数的形式参数（指参数的个数、类型或者顺序）必须不同，也就是说用同一个运算符完成不同的运算功能。");
    ui->textEdit->append("两个重载函数必须在下列一个或两个方面有所区别：");
    ui->textEdit->append("1、函数的参数个数不同。");
    ui->textEdit->append("2、函数的参数类型不同或者参数类型顺序不同，");
    ui->textEdit->append("C++的这种编程机制给编程者极大的方便，不需要为功能相似、参数不同的函数选用不同的函数名，也增强了程序的可读性。");
    ui->textEdit->append("");
    ui->textEdit->append("C++运算符重载的相关规定如下：");
    ui->textEdit->append("（1）不能改变运算符的优先级；");
    ui->textEdit->append("（2）不能改变运算符的结合型；");
    ui->textEdit->append("（3）默认参数不能和重载的运算符一起使用；");
    ui->textEdit->append("（4）不能改变运算符的操作数的个数；");
    ui->textEdit->append("（5）不能创建新的运算符，只有已有运算符可以被重载；");
    ui->textEdit->append("（6）运算符作用于C++内部提供的数据类型时，原来含义保持不变。");
    ui->textEdit->append("");
    ui->textEdit->append("函数重载举例：");
    ui->textEdit->append("int Max （int，int）； //返回两个整数的最大值；");
    ui->textEdit->append("int Max （const vector <int> &）； //返回vector容器中的最大值；");
    ui->textEdit->append("int Max （const matrix &）； //返回matrix引用的最大值；");
    ui->textEdit->moveCursor(QTextCursor::Start,QTextCursor::MoveAnchor);
}
void function::showyufa()
{
    author a;
    a.cdexshow();
    close();
}
void function::showshuju()
{
    author a;
    a.shujushow();
    close();
}
void function::showhead()
{
    author a;
    a.headinfoshow();
    close();
}
void function::invite_set()
{
    int INFO;

    QSettings invi("./data.ini",QSettings::IniFormat);
    QString info=invi.value("/function/wucanhanshu").toString();
    INFO=info.toInt();


    if(max<INFO)
    {
        max=INFO;
        maxS="wucanhanshu";
    }
    info=invi.value("/function/daicanhanshu").toString();

    INFO=info.toInt();

    if(max<INFO)
    {
        max=INFO;
        maxS="daicanhanshu";
    }
    info=invi.value("/function/yinyong").toString();

    INFO=info.toInt();

    if(max<INFO)
    {
        max=INFO;
        maxS="yinyong";
    }
    info=invi.value("/function/zhizhenhanshu").toString();

    INFO=info.toInt();

    if(max<INFO)
    {
        max=INFO;
        maxS="zhizhenhanshu";
    }
    info=invi.value("/function/xuhanshu").toString();

    INFO=info.toInt();

    if(max<INFO)
    {
        max=INFO;
        maxS="xuhanshu";
    }
    info=invi.value("/function/chunxuhanshu").toString();

    INFO=info.toInt();

    if(max<INFO)
    {
        max=INFO;
        maxS="chunxuhanshu";
    }
    info=invi.value("/function/diguihanshu").toString();

    INFO=info.toInt();

    if(max<INFO)
    {
        max=INFO;
        maxS="diguihanshu";
    }
    info=invi.value("/function/gouzaohanshu").toString();

    INFO=info.toInt();

    if(max<INFO)
    {
        max=INFO;
        maxS="gouzaohanshu";
    }
    info=invi.value("/function/xigouhanshu").toString();

    INFO=info.toInt();

    if(max<INFO)
    {
        max=INFO;
        maxS="xigouhanshu";
    }
    info=invi.value("/function/lei").toString();

    INFO=info.toInt();

    if(max<INFO)
    {
        max=INFO;
        maxS="lei";
    }
    info=invi.value("/function/jilei").toString();

    INFO=info.toInt();

    if(max<INFO)
    {
        max=INFO;
        maxS="jilei";
    }
    info=invi.value("/function/youyuan").toString();

    INFO=info.toInt();

    if(max<INFO)
    {
        max=INFO;
        maxS="youyuan";
    }
}
void function::invite()
{
    if(maxS=="wucanhanshu")
    {
        voidfunc();
        maxS="无参函数";
    }
    else if(maxS=="daicanhanshu")
    {
        havefunc();
        maxS="带参函数";
    }
    else if(maxS=="yinyong")
    {
        YINYONG();
        maxS="引用";
    }
    else if(maxS=="zhizhenhanshu")
    {
        ZHIZHENFUNC();
        maxS="指针函数";
    }
    else if(maxS=="xuhanshu")
    {
        XUHANSHU();
        maxS="虚函数";
    }
    else if(maxS=="chunxuhanshu")
    {
        CHUNXUHANSHU();
        maxS="纯虚函数";
    }
    else if(maxS=="diguihanshu")
    {
        DIGUI();
        maxS="递归函数";
    }
    else if(maxS=="gouzaohanshu")
    {
        GOUZAO();
        maxS="构造函数";
    }
    else if(maxS=="xigouhanshu")
    {
        XIGOU();
        maxS="析构函数";
    }
    else if(maxS=="lei")
    {
        CLASS();
        maxS="类";
    }
    else if(maxS=="jilei")
    {
        JILEI();
        maxS="基类";
    }
    else if(maxS=="youyuan")
    {
        FRIEND();
        maxS="友元";
    }
}
void function::invite_judge()
{

   invite_set();
   if(max!=0)
   {
       invite();

       QString a1;
       a1="你最常查看的搜索项是："+maxS+"\n现在开始自动推送";
       QMessageBox::about(NULL, "智能推荐", a1);

   }
}
void function::showcreate()
{
    hide();
    author a;
    a.createshow();
}
