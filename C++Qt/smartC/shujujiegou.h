﻿#ifndef SHUJUJIEGOU_H
#define SHUJUJIEGOU_H

#include <QMainWindow>

namespace Ui {
class shujujiegou;
}

class shujujiegou : public QMainWindow
{
    Q_OBJECT

public:
    explicit shujujiegou(QWidget *parent = 0);
    ~shujujiegou();

private slots:
    void on_pushButton_exit_clicked();
    void showcdex();
    void showhead();
    void init();
    void Show();
    void SHUNXU();//顺序表
    void on_tableView_clicked(const QModelIndex &index);
    void DANLIANBIAO();//单链表
    void SHUANGLIANBIAO();//双链表
    void AVL();//平衡二叉树
    void tree();//简单树
    void erchashu();//二叉树
    void redblack();
    void on_lineEdit_textChanged(const QString &arg1);
    void ZHAN();
    void DUI();
    void DUILIE();
    void huffman();//哈夫曼
    void XUNHUAN();
    void showfunc();
    void invite();
    void invite_set();
    void invite_judge();
    void showcreate();
private:
    Ui::shujujiegou *ui;
    QString maxS;
    int max=0;
};

#endif // SHUJUJIEGOU_H
