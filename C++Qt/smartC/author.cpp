﻿#include "author.h"
#include "ui_author.h"
#include"cdex.h"

author::author(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::author)
{
    ui->setupUi(this);
    QPixmap pixmap = QPixmap("./image/author.jpg").scaled(this->size());//窗口背景图片 始
    QPalette  palette (this->palette());
    palette .setBrush(QPalette::Background, QBrush(pixmap));
    this-> setPalette( palette );//窗口背景图片 终
    setFixedSize(453,297);//设置窗口不可拉伸

}

author::~author()
{
    delete ui;
}
void author::cdexshow()
{
    Cdex=new cdex;

    Cdex->show();

}
void author::headinfoshow()
{
    headin=new headinfo;
    headin->show();

}

void author::shujushow()
{
    shuju=new shujujiegou;
    shuju->show();
}
void author::funcshow()
{
    func=new function;
    func->show();
}
void author::createshow()
{
    create=new createcode;
    create->show();
}
