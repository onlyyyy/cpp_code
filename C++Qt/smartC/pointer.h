﻿#ifndef POINTER_H
#define POINTER_H

#include <QMainWindow>

namespace Ui {
class pointer;
}

class pointer : public QMainWindow
{
    Q_OBJECT

public:
    explicit pointer(QWidget *parent = 0);
    ~pointer();

private slots:
    void on_pushButton_clicked();

private:
    Ui::pointer *ui;
};

#endif // POINTER_H
