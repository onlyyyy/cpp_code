﻿#include "mainwindow.h"
#include <QApplication>
#include<QDebug>
#include<QSplashScreen>//Qt启动画面支持

bool opendatabase();
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QPixmap pixmap("./image/splash.png");
    QSplashScreen splash(pixmap);

    splash.show();
    a.processEvents();

    MainWindow w;
    w.show();
    splash.close();
    opendatabase();
    return a.exec();
}
bool opendatabase()
{
    QSqlDatabase mydb=QSqlDatabase::addDatabase("QSQLITE");
    mydb.setDatabaseName("D:\\sqlite\\Qt.db");//平时debug正常用
    //mydb.setDatabaseName("./Qt.db");//release用
    if(mydb.open())
    {
        qDebug()<<"open success";
        return true;
    }
    else
    {
        qDebug()<<"open failed";
        return false;
    }
}
