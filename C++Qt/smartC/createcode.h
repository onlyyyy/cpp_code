﻿#ifndef CREATECODE_H
#define CREATECODE_H
#include<QStackedWidget>
#include <QMainWindow>
#include<QListWidget>
#pragma execution_character_set("utf-8")//设置中文= =
#include<QLabel>
#include<QMessageBox>
#include<QDebug>
#include"remind.h"

namespace Ui {
class createcode;
}

class createcode : public QMainWindow
{
    Q_OBJECT

public:
    explicit createcode(QWidget *parent = 0);
    ~createcode();
    QMessageBox msg;
private slots:
    void on_pushButton_clicked();

    void on_listWidget_auto_itemClicked(QListWidgetItem *item);

    void on_pushButton_showarray_clicked();

    void on_pushButton_2arrayshow_clicked();

    void on_pushButton_random_clicked();
    void showcdex();
    void showshuju();
    void showfunc();
    void showhead();
    void showfinished();

    void on_pushButton_sum_clicked();

private:
    Ui::createcode *ui;
    QStackedWidget *stack;
    QListWidget *list;
    remind *remi;
};

#endif // CREATECODE_H
