/********************************************************************************
** Form generated from reading UI file 'md5_check.ui'
**
** Created by: Qt User Interface Compiler version 5.12.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MD5_CHECK_H
#define UI_MD5_CHECK_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MD5_check
{
public:
    QWidget *centralwidget;
    QPushButton *pushButton_createMD5;
    QLabel *label;
    QLabel *label_2;
    QLineEdit *lineEdit_input;
    QLineEdit *lineEdit_result;
    QPushButton *pushButton;
    QPushButton *pushButton_createMD5ofFile;
    QLineEdit *lineEdit_result_file;
    QLabel *label_3;
    QLineEdit *lineEdit_result_check;
    QLabel *label_4;
    QPushButton *pushButton_check;
    QComboBox *comboBox_check;
    QLabel *label_5;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MD5_check)
    {
        if (MD5_check->objectName().isEmpty())
            MD5_check->setObjectName(QString::fromUtf8("MD5_check"));
        MD5_check->resize(637, 414);
        centralwidget = new QWidget(MD5_check);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        pushButton_createMD5 = new QPushButton(centralwidget);
        pushButton_createMD5->setObjectName(QString::fromUtf8("pushButton_createMD5"));
        pushButton_createMD5->setGeometry(QRect(80, 30, 91, 31));
        label = new QLabel(centralwidget);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(220, 40, 54, 12));
        label_2 = new QLabel(centralwidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(220, 100, 54, 12));
        lineEdit_input = new QLineEdit(centralwidget);
        lineEdit_input->setObjectName(QString::fromUtf8("lineEdit_input"));
        lineEdit_input->setGeometry(QRect(290, 30, 221, 31));
        lineEdit_result = new QLineEdit(centralwidget);
        lineEdit_result->setObjectName(QString::fromUtf8("lineEdit_result"));
        lineEdit_result->setGeometry(QRect(290, 90, 221, 31));
        pushButton = new QPushButton(centralwidget);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setGeometry(QRect(280, 320, 75, 23));
        pushButton_createMD5ofFile = new QPushButton(centralwidget);
        pushButton_createMD5ofFile->setObjectName(QString::fromUtf8("pushButton_createMD5ofFile"));
        pushButton_createMD5ofFile->setGeometry(QRect(70, 140, 91, 31));
        lineEdit_result_file = new QLineEdit(centralwidget);
        lineEdit_result_file->setObjectName(QString::fromUtf8("lineEdit_result_file"));
        lineEdit_result_file->setGeometry(QRect(290, 150, 221, 31));
        label_3 = new QLabel(centralwidget);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setGeometry(QRect(210, 150, 61, 21));
        lineEdit_result_check = new QLineEdit(centralwidget);
        lineEdit_result_check->setObjectName(QString::fromUtf8("lineEdit_result_check"));
        lineEdit_result_check->setGeometry(QRect(290, 230, 221, 31));
        label_4 = new QLabel(centralwidget);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setGeometry(QRect(210, 230, 71, 31));
        pushButton_check = new QPushButton(centralwidget);
        pushButton_check->setObjectName(QString::fromUtf8("pushButton_check"));
        pushButton_check->setGeometry(QRect(70, 220, 91, 41));
        comboBox_check = new QComboBox(centralwidget);
        comboBox_check->addItem(QString());
        comboBox_check->addItem(QString());
        comboBox_check->setObjectName(QString::fromUtf8("comboBox_check"));
        comboBox_check->setGeometry(QRect(540, 230, 69, 22));
        label_5 = new QLabel(centralwidget);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setGeometry(QRect(70, 310, 161, 31));
        MD5_check->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MD5_check);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 637, 23));
        MD5_check->setMenuBar(menubar);
        statusbar = new QStatusBar(MD5_check);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        MD5_check->setStatusBar(statusbar);

        retranslateUi(MD5_check);

        QMetaObject::connectSlotsByName(MD5_check);
    } // setupUi

    void retranslateUi(QMainWindow *MD5_check)
    {
        MD5_check->setWindowTitle(QApplication::translate("MD5_check", "MainWindow", nullptr));
        pushButton_createMD5->setText(QApplication::translate("MD5_check", "\347\224\237\346\210\220MD5", nullptr));
        label->setText(QApplication::translate("MD5_check", "\350\276\223\345\205\245", nullptr));
        label_2->setText(QApplication::translate("MD5_check", "MD5:", nullptr));
        pushButton->setText(QApplication::translate("MD5_check", "\351\200\200\345\207\272", nullptr));
        pushButton_createMD5ofFile->setText(QApplication::translate("MD5_check", "\347\224\237\346\210\220\346\226\207\344\273\266\347\232\204MD5", nullptr));
        label_3->setText(QApplication::translate("MD5_check", "\346\226\207\344\273\266\347\232\204MD5", nullptr));
        label_4->setText(QApplication::translate("MD5_check", "\346\240\241\351\252\214\345\256\214\346\225\264\346\200\247", nullptr));
        pushButton_check->setText(QApplication::translate("MD5_check", "\346\240\241\351\252\214\345\256\214\346\225\264\346\200\247", nullptr));
        comboBox_check->setItemText(0, QApplication::translate("MD5_check", "\345\255\227\346\256\265", nullptr));
        comboBox_check->setItemText(1, QApplication::translate("MD5_check", "\346\226\207\344\273\266", nullptr));

        label_5->setText(QApplication::translate("MD5_check", "\347\224\237\346\210\220\347\232\204MD5\344\274\232\350\207\252\345\212\250\344\277\235\345\255\230\345\210\260\345\211\252\350\264\264\346\235\277", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MD5_check: public Ui_MD5_check {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MD5_CHECK_H
