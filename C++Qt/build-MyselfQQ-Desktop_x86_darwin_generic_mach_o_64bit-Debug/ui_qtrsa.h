/********************************************************************************
** Form generated from reading UI file 'qtrsa.ui'
**
** Created by: Qt User Interface Compiler version 5.12.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_QTRSA_H
#define UI_QTRSA_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_QtRSA
{
public:
    QWidget *centralwidget;
    QPushButton *pushButton_jiami;
    QPushButton *pushButton_jiemi;
    QLabel *label;
    QLabel *label_2;
    QLineEdit *lineEdit_input;
    QLineEdit *lineEdit_result;
    QLineEdit *lineEdit_e;
    QLineEdit *lineEdit_d;
    QLineEdit *lineEdit_n;
    QLabel *label_3;
    QLabel *label_4;
    QLabel *label_5;
    QPushButton *pushButton_exit;
    QLabel *label_10;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *QtRSA)
    {
        if (QtRSA->objectName().isEmpty())
            QtRSA->setObjectName(QString::fromUtf8("QtRSA"));
        QtRSA->resize(654, 442);
        centralwidget = new QWidget(QtRSA);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        pushButton_jiami = new QPushButton(centralwidget);
        pushButton_jiami->setObjectName(QString::fromUtf8("pushButton_jiami"));
        pushButton_jiami->setGeometry(QRect(70, 30, 91, 31));
        pushButton_jiemi = new QPushButton(centralwidget);
        pushButton_jiemi->setObjectName(QString::fromUtf8("pushButton_jiemi"));
        pushButton_jiemi->setGeometry(QRect(70, 110, 91, 31));
        label = new QLabel(centralwidget);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(210, 40, 61, 21));
        label_2 = new QLabel(centralwidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(210, 110, 61, 21));
        lineEdit_input = new QLineEdit(centralwidget);
        lineEdit_input->setObjectName(QString::fromUtf8("lineEdit_input"));
        lineEdit_input->setGeometry(QRect(290, 30, 181, 31));
        lineEdit_result = new QLineEdit(centralwidget);
        lineEdit_result->setObjectName(QString::fromUtf8("lineEdit_result"));
        lineEdit_result->setGeometry(QRect(290, 110, 181, 31));
        lineEdit_e = new QLineEdit(centralwidget);
        lineEdit_e->setObjectName(QString::fromUtf8("lineEdit_e"));
        lineEdit_e->setGeometry(QRect(150, 180, 113, 20));
        lineEdit_d = new QLineEdit(centralwidget);
        lineEdit_d->setObjectName(QString::fromUtf8("lineEdit_d"));
        lineEdit_d->setGeometry(QRect(150, 230, 113, 20));
        lineEdit_n = new QLineEdit(centralwidget);
        lineEdit_n->setObjectName(QString::fromUtf8("lineEdit_n"));
        lineEdit_n->setGeometry(QRect(150, 280, 113, 20));
        label_3 = new QLabel(centralwidget);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setGeometry(QRect(90, 190, 54, 12));
        label_4 = new QLabel(centralwidget);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setGeometry(QRect(90, 230, 54, 12));
        label_5 = new QLabel(centralwidget);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setGeometry(QRect(90, 280, 54, 12));
        pushButton_exit = new QPushButton(centralwidget);
        pushButton_exit->setObjectName(QString::fromUtf8("pushButton_exit"));
        pushButton_exit->setGeometry(QRect(300, 340, 75, 23));
        label_10 = new QLabel(centralwidget);
        label_10->setObjectName(QString::fromUtf8("label_10"));
        label_10->setGeometry(QRect(20, 152, 111, 20));
        QtRSA->setCentralWidget(centralwidget);
        menubar = new QMenuBar(QtRSA);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 654, 23));
        QtRSA->setMenuBar(menubar);
        statusbar = new QStatusBar(QtRSA);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        QtRSA->setStatusBar(statusbar);

        retranslateUi(QtRSA);

        QMetaObject::connectSlotsByName(QtRSA);
    } // setupUi

    void retranslateUi(QMainWindow *QtRSA)
    {
        QtRSA->setWindowTitle(QApplication::translate("QtRSA", "MainWindow", nullptr));
        pushButton_jiami->setText(QApplication::translate("QtRSA", "\345\212\240\345\257\206", nullptr));
        pushButton_jiemi->setText(QApplication::translate("QtRSA", "\350\247\243\345\257\206", nullptr));
        label->setText(QApplication::translate("QtRSA", "\346\230\216\346\226\207", nullptr));
        label_2->setText(QApplication::translate("QtRSA", "\345\257\206\346\226\207", nullptr));
        label_3->setText(QApplication::translate("QtRSA", "\345\205\254\351\222\245e", nullptr));
        label_4->setText(QApplication::translate("QtRSA", "\347\247\201\351\222\245d", nullptr));
        label_5->setText(QApplication::translate("QtRSA", "\345\257\206\351\222\245\351\205\215\345\257\271n", nullptr));
        pushButton_exit->setText(QApplication::translate("QtRSA", "\345\205\263\351\227\255", nullptr));
        label_10->setText(QApplication::translate("QtRSA", "\347\250\213\345\272\217\347\224\237\346\210\220\347\232\204\345\257\206\351\222\245\345\257\271", nullptr));
    } // retranslateUi

};

namespace Ui {
    class QtRSA: public Ui_QtRSA {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_QTRSA_H
