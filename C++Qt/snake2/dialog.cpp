#include "dialog.h"
#include "ui_dialog.h"
#include<string>
using namespace std;
extern string nam[3];
extern int sco[3];
Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
    ui->label1_name->setText(QString::fromStdString(nam[0]));
    ui->label1_score->setText(QString::number(sco[0]));
    ui->label2_name->setText(QString::fromStdString(nam[1]));
    ui->label2_score->setText(QString::number(sco[1]));
    ui->label3_name->setText(QString::fromStdString(nam[2]));
    ui->label3_score->setText(QString::number(sco[2]));

}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::on_pushButton_2_clicked()
{
    accept();
}

void Dialog::on_pushButton_clicked()
{
    nam[0]=nam[1]=nam[2]="UnknowPlayer";
    sco[0]=sco[1]=sco[2]=0;
    ui->label1_name->setText(QString::fromStdString(nam[0]));
    ui->label1_score->setText(QString::number(sco[0]));
    ui->label2_name->setText(QString::fromStdString(nam[1]));
    ui->label2_score->setText(QString::number(sco[1]));
    ui->label3_name->setText(QString::fromStdString(nam[2]));
    ui->label3_score->setText(QString::number(sco[2]));
}
