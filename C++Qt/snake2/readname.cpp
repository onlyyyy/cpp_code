#include "readname.h"
#include "ui_readname.h"
#include <QRegExp>
readname::readname(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::readname)
{
    ui->setupUi(this);
    QRegExp regExp("^[A-Za-z0-9_]+$");
    ui->lineEdit->setValidator(new QRegExpValidator(regExp, this));
}

readname::~readname()
{
    delete ui;
}



void readname::on_exitButton_clicked()
{
    emit sendData(ui->lineEdit->text().toStdString());
    accept();
}
