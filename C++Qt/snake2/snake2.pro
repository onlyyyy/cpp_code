#-------------------------------------------------
#
# Project created by QtCreator 2013-11-17T20:29:55
#
#-------------------------------------------------

QT       += core gui
QT       +=multimedia
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = snake2
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    dialog.cpp \
    readname.cpp

HEADERS  += mainwindow.h \
    dialog.h \
    readname.h

FORMS    += mainwindow.ui \
    dialog.ui \
    readname.ui

RESOURCES += \
    sourc.qrc

RC_FILE += \
    ico.rc
