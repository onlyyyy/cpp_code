#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include<QWidget>
#include<QVector>
#include<QLabel>
#include<QSound>
#include<string>
using namespace std;
#define WIDTH 30
#define HEIGHT 20
int  const LEVEL[]={0,1000,500,200,100,50};

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:

    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    enum direction{UP,DOWN,LEFT,RIGHT};


private slots:
    void automove();

    void receiveData(string rec);

    void on_action_Quit_triggered();

    void on_action_New_triggered();

    void on_action_Pause_triggered();

    void on_action_Continue_triggered();

    void on_action_level1_triggered();

    void on_action_level2_triggered();

    void on_action_level3_triggered();

    void on_action_level4_triggered();

    void on_action_level5_triggered();

    void on_action_help_triggered();

    void on_action_author_triggered();

    void on_action_Score_triggered();

    void addscore();

private:
    Ui::MainWindow *ui;
    QPoint apple;
    QVector<QPoint> snake;
    QVector< QVector<int> > map;

    enum direction direc;
    int speed;
    int level;
    QTimer *timer;
    int score;
    void eat();
    void move(enum direction direc);
    void randapple();
    void gameover();
    void loadscore();
    void getname();
    void save();
    void init_statuBar();
    QLabel *first_statusLabel,*second_statusLabel;
    string player;
signals:
    void gotin();
protected:
    void paintEvent(QPaintEvent *event);
    void keyPressEvent(QKeyEvent *event);
};

#endif // MAINWINDOW_H
