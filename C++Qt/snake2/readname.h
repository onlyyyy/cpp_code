#ifndef READNAME_H
#define READNAME_H

#include <QDialog>
#include<string>
using namespace std;
namespace Ui {
class readname;
}

class readname : public QDialog
{
    Q_OBJECT

public:
    explicit readname(QWidget *parent = 0);
    ~readname();

private slots:

    void on_exitButton_clicked();

signals:
    void sendData(string);
private:
    Ui::readname *ui;
};

#endif // READNAME_H
